//
//  Channel.swift
//  YMGroup
//
//  Created by phuongzzz on 10/18/18.
//  Copyright © 2018 phuongzzz. All rights reserved.
//

import Foundation

struct Channel: Decodable {
    public private(set) var channelTitle: String!
    public private(set) var channelDescription: String!
    public private(set) var id: String!
}
