//
//  LoginVC.swift
//  YMGroup
//
//  Created by phuongzzz on 10/13/18.
//  Copyright © 2018 phuongzzz. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    @IBAction func closePressed() {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func createAccountBtnPressed() {
        performSegue(withIdentifier: TO_CREATE_ACCOUNT, sender: nil)
    }
    
    @IBAction func loginBtnPressed() {
        spinner.isHidden = false
        spinner.startAnimating()
        
        guard let email = usernameField.text, usernameField.text != "" else { return }
        guard let password = passwordField.text, passwordField.text != "" else { return }
        
        AuthService.instance.loginUser(email: email, password: password) { (success) in
            if success {
                AuthService.instance.findUserByEmail(completion: { (success) in
                    if success {
                        NotificationCenter.default.post(name: NOTIF_USER_DATA_DID_CHANGE, object: nil)
                        self.spinner.isHidden = true
                        self.spinner.stopAnimating()
                        self.dismiss(animated: true, completion: nil)
                    }
                })
            }
        }
    }
    
    func setupView() {
        spinner.isHidden = true
        usernameField.attributedPlaceholder = NSAttributedString(string: "enter your email", attributes: [NSAttributedString.Key.foregroundColor: PLACEHOLDER_COLOR])
        passwordField.attributedPlaceholder = NSAttributedString(string: "enter your password", attributes: [NSAttributedString.Key.foregroundColor: PLACEHOLDER_COLOR])
    }
}
