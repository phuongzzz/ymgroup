//
//  ProfileVC.swift
//  YMGroup
//
//  Created by phuongzzz on 10/17/18.
//  Copyright © 2018 phuongzzz. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController {
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userEmail: UILabel!
    @IBOutlet weak var bgView: UIView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    @IBAction func closeModalBtnPressed() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func logoutBtnPressed() {
        UserDataService.instance.logoutUser()
        NotificationCenter.default.post(name: NOTIF_USER_DATA_DID_CHANGE, object: nil)
        dismiss(animated: true, completion: nil)
    }
    
    func setupView() {
        self.userName.text = UserDataService.instance.name
        self.userEmail.text = UserDataService.instance.email
        self.profileImage.image = UIImage(named: UserDataService.instance.avatarName)
        self.profileImage.backgroundColor = UserDataService.instance.returnUIColor(component: UserDataService.instance.avatarColor)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(ProfileVC.handleTap))
        view.addGestureRecognizer(tap)
    }
    
    @objc func handleTap() {
        self.dismiss(animated: true, completion: nil)
    }
}
