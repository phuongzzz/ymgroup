//
//  AddChannelVC.swift
//  YMGroup
//
//  Created by phuongzzz on 10/18/18.
//  Copyright © 2018 phuongzzz. All rights reserved.
//

import UIKit

class AddChannelVC: UIViewController {
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var descriptionTextField: UITextField!
    @IBOutlet weak var bgView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    @IBAction func closeModalBtnPressed() {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func createChannelBtnPressed() {
        guard let channelName = nameTextField.text, nameTextField.text != "" else { return }
        guard let channelDescription = descriptionTextField.text, descriptionTextField.text != "" else { return }
        SocketService.instance.addChannel(channelName: channelName, channelDescription: channelDescription) { (success) in
            if success {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    func setupView() {
        let closeTouch = UITapGestureRecognizer(target: self, action: #selector(AddChannelVC.handleTap))
        bgView.addGestureRecognizer(closeTouch)
        nameTextField.attributedPlaceholder = NSAttributedString(string: "channel name", attributes: [NSAttributedString.Key.foregroundColor: PLACEHOLDER_COLOR])
        descriptionTextField.attributedPlaceholder = NSAttributedString(string: "channel description name", attributes: [NSAttributedString.Key.foregroundColor: PLACEHOLDER_COLOR])
    }
    
    @objc func handleTap() {
        dismiss(animated: true, completion: nil)
    }
}
