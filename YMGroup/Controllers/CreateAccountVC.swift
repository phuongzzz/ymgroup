//
//  CreateAccountVC.swift
//  YMGroup
//
//  Created by phuongzzz on 10/13/18.
//  Copyright © 2018 phuongzzz. All rights reserved.
//

import UIKit

class CreateAccountVC: UIViewController {
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if UserDataService.instance.avatarName != "" {
            userImage.image = UIImage(named: UserDataService.instance.avatarName)
            avatarName = UserDataService.instance.avatarName
            if avatarName.contains("light") && bgColor == nil {
                userImage.backgroundColor = UIColor.lightGray
            }
        }
    }
    
    // TEMPORARY VARIABLES
    var avatarName = "profileDefault"
    var defaultAvatarColor = "[0.5, 0.5, 0.5, 1]" // gray color
    var bgColor: UIColor?
    
    @IBAction func createAccountBtnPressed() {
        spinner.isHidden = false
        spinner.startAnimating()
        guard let email = emailField.text, emailField.text != "" else { return }
        guard let name = nameField.text, nameField.text != "" else { return }
        guard let password = passwordField.text, passwordField.text != "" else { return }
        
        AuthService.instance.registerUser(email: email, password: password) {
            (success) in
            if success {
                AuthService.instance.loginUser(email: email, password: password, completion: { (success) in
                    if (success) {
                        AuthService.instance.createUser(name: name, email: email, avatarName: self.avatarName, avatarColor: self.defaultAvatarColor, completion: { (success) in
                            if success {
                                self.spinner.isHidden = true
                                self.spinner.stopAnimating()
                                self.performSegue(withIdentifier: UNWIND_TO_CHANNEL, sender: nil)
                                NotificationCenter.default.post(name: NOTIF_USER_DATA_DID_CHANGE, object: nil)
                            }
                        })
                    }
                })
            }
        }
    }
    
    @IBAction func closeButtonPressed() {
        performSegue(withIdentifier: UNWIND_TO_CHANNEL, sender: nil)
    }
    
    @IBAction func pickAvatarPressed() {
        performSegue(withIdentifier: TO_AVATAR_PICKER, sender: nil)
    }
    
    @IBAction func pickBackgroundBtnPressed() {
        let red = CGFloat(arc4random_uniform(255)) / 255
        let green = CGFloat(arc4random_uniform(255)) / 255
        let blue = CGFloat(arc4random_uniform(255)) / 255
        
        bgColor = UIColor(red: red, green: green, blue: blue, alpha: 1.0)
        defaultAvatarColor = "[\(red), \(green), \(blue), 1]"
        UIView.animate(withDuration: 0.25) {
            self.userImage.backgroundColor = self.bgColor
        }
    }
    
    func setupView() {
        spinner.isHidden = true
        nameField.attributedPlaceholder = NSAttributedString(string: "username", attributes: [NSAttributedString.Key.foregroundColor: PLACEHOLDER_COLOR])
        emailField.attributedPlaceholder = NSAttributedString(string: "email", attributes: [NSAttributedString.Key.foregroundColor: PLACEHOLDER_COLOR])
        passwordField.attributedPlaceholder = NSAttributedString(string: "enter your password", attributes: [NSAttributedString.Key.foregroundColor: PLACEHOLDER_COLOR])
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(CreateAccountVC.handleTap))
        view.addGestureRecognizer(tap)
    }
    
    @objc func handleTap() {
        view.endEditing(true)
    }
}
